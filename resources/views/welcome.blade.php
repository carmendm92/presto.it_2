<x-layout>
    <div class="container">
        <div class="row justify-content-center my-2">
            @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif

            @if(session('submit'))
            <div class="alert alert-success">
                {{ session('submit') }}
            </div>
            @endif

            @if(session('error'))
            <div class="alert alert-warning">
                {{ session('error') }}
            </div>
            @endif

            <div class="col-12 p-4 text-center fw-bolder mt-5">
                <h1>
                  {{ __('ui.categorie')}}
                </h1>
            </div>
            
            @foreach ($categories as $category)
            <div class="col-12 col-sm-5 col-lg-2 m-2 text-center ">
                <a class="btn card-category text-white" href="{{route('announcement.category',compact('category'))}}">
                    {{$category->name}}
                </a>
                
            </div>
            @endforeach
            
            

            <div class="col-12 p-4 text-center fw-bolder mt-5">
                <h1>
                    {{ __('ui.recenti')}}
                </h1>
            </div>

            @foreach ($announcements as $announcement)
            <div class="col-12 col-md-6 col-xl-3 my-5">
                <x-card
                nome="{{$announcement->name}}"
                immagine="{{$announcement->images->first()?$announcement->images->first()->getUrl(400,400) : 'https://picsum.photos/400'}}"
                categoria="{{$announcement->category->name}}"     
                descrizione="{{$announcement->body}}"
                autore="{{$announcement->user->name}}"
                datacreazione="{{$announcement->created_at->format('d.m.Y')}}"
                routeCategoria="{{route('announcement.category', ['category'=>$announcement->category->id])}}"
                route="{{route('announcement.show', compact('announcement'))}}"
                prezzo="{{$announcement->price}}"
                >
                </x-card>
            </div>
            @endforeach
        </div>
    </div>
</x-layout>