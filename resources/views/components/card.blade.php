<div class="border border-dark card">

    <div class="row justify-content-center align-items-center">
        <div class="col-12">
            <h4 class="text-center card_title fw-bolder p-2 m-2">
                {{$nome}}
            </h4>
        </div>
        <div class="col-12 col-md-6 text-center">
            <div class="text-center d-flex justify-content-center align-items-center">
                <img src="{{$immagine}}" class="text-center p-1 border border-dark" alt="{{$nome}}" style="max-height:150px">
            </div>
        </div>
        <div class="col-12 col-md-8 text-center">
            <a href="{{$routeCategoria ?? ''}}" class="btn text-decoration-underline">#{{$categoria ?? ''}}</a>
            <div class="text-center mb-2">
                <a class="p-2 btn btn-leggi" href="{{$route ?? ''}}"> {{ __('ui.leggi')}}</a>
                <div class="p-2 text-center">
                    <p>{{ __('ui.prezzo')}}<br></p>
                    <p class="text-danger">{{$prezzo ?? ''}}€ </p>
                </div>
                <i>{{$datacreazione}} - {{$autore}}</i>
            </div>
        </div>
    </div>
</div>