<div class="container-fluid header d-flex align-items-center">
    <div class="container ">
        <div class="row text-center justify-content-center align-items-center">
            <div class="col-12 text-center"> 
                <a class="titolo fw-bolder fa-5x" href="{{route('homepage')}}">Presto.it</a>
            </div>
            <div class="col-12 d-flex justify-content-center">
                <form action="{{route('search')}}" method="GET">
                   <div class=" input-group container-searchbar">
                        <input type="text" class="searchbar p-2" placeholder=" {{ __('ui.cerca')}}" name="q" aria-label="Cerca">
                        <button class="btn" type="submit" id="button-addon2"><i class="fa-solid fa-magnifying-glass iconsearch text-dark"></i></button>
                    </div> 
                </form>

                
            </div>
        </div>
    </div>
</div>