<footer class="container-fluid d-flex align-items-center justify-content-center footer">
    <div class="container">
        <div class="row">
            <div class="col-12 social">
                <p class="text-white text-center">
                    © 2021 Presto.it - P.IVA 99999999999</p>
            </div>


            <div class="col-12">
                <div class="container-icon d-flex justify-content-center">
                    <a class="nav-link " href="#">
                        <i class="fa-brands fa-facebook-f link-footer fa-2x"></i>
                    </a>
                    <a class="nav-link " href="#">
                        <i class="fa-brands fa-twitter link-footer fa-2x"></i>
                    </a>
                    <a class="nav-link " href="#">
                        <i class="fa-brands fa-pinterest-p link-footer fa-2x"></i>
                    </a>
                    <a class="nav-link" href="#">
                        <i class="fa-brands fa-instagram link-footer fa-2x"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>