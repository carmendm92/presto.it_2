<nav class="navbar navbar-expand-lg navbar-presto sticky-top">
    <div class="container-fluid">
      <a class="navbar-brand text-white fw-bolder" href="{{route('homepage')}}"> <img src="/img/logo.png" alt=""></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class=" text-white fa-solid fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
        <ul class="navbar-nav m-auto">
          <li class="nav-item">
            <a class="nav-link active text-white fw-bolder" aria-current="page" href="{{route('homepage')}}">Home</a>
          </li>
          @guest
          <li class="nav-item">
            <a class="nav-link text-white fw-bolder" href="{{route('login')}}">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white fw-bolder" href="{{route('register')}}">{{ __('ui.registrati')}}</a>
          </li>
          @endguest
          @auth
          <li class="nav-item">
            <a class="nav-link text-white fw-bolder" href="{{route('announcement.create')}}"> {{ __('ui.inserisci')}}</a>
          </li>          
        </ul>
          @endauth
      @auth
        <div class="container-pannel-user" id="navbarSupportedContent">
          <ul class="navbar-nav ">
            @if(Auth::user()->is_revisor)
            <li class="new-item">
              <a class=" nav-link btn btn-notifica position-relative fw-bolder me-4" href="{{route('homepage.revisor')}}">
                {{ __('ui.pannello')}}
              @if(\App\Models\Announcement::ToBeRevisionedCount() != 0)
              <span class="position-absolute top-0 start-100 border border-white translate-middle badge rounded-pill bg-danger">
                {{\App\Models\Announcement::ToBeRevisionedCount()}}
              </span>
              @endif
            </a>
            </li>
            @endif     
      @endauth 
          </ul>
        </div>
{{-- dropdown lingua --}}
        <div id="navbarSupportedContent">
          <li class="nav-link dropdown">
            <a class="nav-link dropdown-toggle text-white fw-bolder" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              {{ __('ui.lingua')}}
            </a>
            <ul class="dropdown-menu language navbar-presto" aria-labelledby="navbarDropdown">
              <li class="nav-item">              
                <form action="{{route('locale', 'it')}}" method="POST">
                  @csrf
                    <button type="submit" class="nav-link btn" style="border:none" >
                      <span class="flag-icon flag-icon-it border border-dark"></span>
                    </button>
                </form>
              </li>
              <li class="nav-item"> 
                <form action="{{route('locale', 'en')}}" method="POST">
                  @csrf
                    <button type="submit" class="nav-link btn" style="background-color:trasparent; border:none">
                      <span class="flag-icon flag-icon-gb border border-dark"></span>
                    </button>
                </form>
              </li>
              <li class="nav-item"> 
                <form action="{{route('locale', 'de')}}" method="POST">
                  @csrf
                    <button type="submit" class="nav-link btn" style="background-color:trasparent; border:none">
                      <span class="flag-icon flag-icon-de border border-dark"></span>
                    </button>
                </form>
              </li>
                
            </ul>
          </li>
        </div> 
      @auth

{{-- dropdown user --}}
        <div id="navbarSupportedContent">
          <li class="nav-link dropdown">
            <a class="nav-link dropdown-toggle text-white fw-bolder" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              {{ __('ui.saluta')}} {{Auth::user()->name}}!
            </a>
            <ul class="dropdown-menu navbar-presto" aria-labelledby="navbarDropdown">
              @if(!Auth::user()->is_revisor)
              <li>              
                <a class="dropdown-item text-white fw-bolder navbar-presto" href="{{route('user.contact')}}">{{ __('ui.lavora')}}</a>
              </li>
              @endif
              @if (Auth::user()->is_revisor)
                <li>
                  <a class="dropdown-item text-white fw-bolder navbar-presto" href="{{route('softdelete')}}">{{ __('ui.cestino')}}</a>
                </li>
              @endif
                <li>
                  <a class="dropdown-item text-white fw-bolder navbar-presto" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('form-logout').submit();">Logout</a></li>
                <form action="{{route('logout')}}" method="POST" id="form-logout">
                  @csrf
                </form>
                <li>
          </li>
        </ul>
      @endauth
    </div>
      </div>
    </div>
  </nav>