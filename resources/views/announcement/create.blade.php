<x-layout>
<div class="container">
    <div class="row justify-content-center">

        <div class="col-12 text-center p-4">
            <h1>{{ __('ui.inserisci')}}</h1>
        </div>
        @if ($errors->any()) 
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
        <div class="col-12 col-md-6 p-3">
            <form method="POST" action="{{route('announcement.store')}}"enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">
                <div class="mb-3">
                  <label for="exampleInputtext1" class="form-label fw-bolder">{{ __('ui.Inserisci il titolo')}}</label>
                  <input type="text" class="form-control border-dark" name="name" value="{{old('name')}}">
                </div>
                 <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label fw-bolder">{{ __('ui.Inserisci descrizione')}}</label>
                    <textarea class="form-control border-dark" id="exampleFormControlTextarea1" name="body" rows="3">{{old('body')}}</textarea>
                  </div>

                  <div class="form-group row">
                    <label for="images" class="col-md-12 col-form-label text-md-left fw-bolder">{{ __('ui.Inserisci immagini')}}</label>
                        <div class="col-12">

                            <div class="dropzone border border-dark" id="drophere"></div>

                        </div>
                 </div>

                 <p class="fw-bolder mt-2">{{ __('ui.Seleziona la categoria')}}</p>
                  <select name="category_id" class=" text-center m-3"> 
                    @foreach ($categories as $category)
                        <option  value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                  </select>
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label fw-bolder">{{ __('ui.Inserisci prezzo')}}</label>
                    <input type="number" class="form-control border-dark" id="exampleInputEmail1" name="price" value="{{old('price')}}">
                 </div>
                <div class="mb-4 text-center ">
                    <button type="submit" class="btn btn-invia">{{ __('ui.invia')}}</button>
                </div>
                <div class="mb-4 text-center ">
                    <a href="{{route('homepage')}}" class=" btn btn-annulla">{{ __('ui.annulla')}}</a>
                </div>
              </form>
        </div>
    </div>
</div>
</x-layout>