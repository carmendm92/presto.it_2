<x-layout>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 text-center">
            <h1 class="fw-bolder p-3">{{$announcement->name}}</h1>
            <a class="btn my-3 text-decoration-underline" href="{{route('announcement.category', ['category'=>$announcement->category->id])}}">
              #{{$announcement->category->name}}</a>
        </div>
        <div class="col-12 col-md-6 mb-3">
          <ol class="carousel-indicators">
            @foreach( $announcement->images as  $image )
               <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
            @endforeach
           </ol>
            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
                <div class="carousel-inner text-center">
                @foreach ($announcement->images as $image)
                  <div class="carousel-item d-block {{ $loop->first ? 'active' : '' }}">
                    <img src="{{$image->getUrl(290, 400)}}">
                  </div>
                @endforeach
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                  <span aria-hidden="true"><i class="text-dark fa-solid fa-circle-chevron-left"></i></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                  <span aria-hidden="true"> <i class=" text-dark fa-solid fa-circle-chevron-right"></i></span>
                  <span class="visually-hidden">Next</span>
                </button>
              </div>
        </div>


        <div class="col-12 col-md-6 p-2 d-flex flex-column  justify-content-center">
          <div class="col-12">
            <div class="p-5 container-body">
              <h4 class="text-center fw-bolder">{{ __('ui.descrizione prodotto')}}</h4>
              <p>{{$announcement->body}}</p>  
            </div>
              <div class="col-12">
                <p class="text-center text-danger fw-bolder mt-2">{{$announcement->price}}€</p>  
              </div>
            </div>
            <div class="col-12 text-end">
              <i>{{$announcement->user->name}} - {{$announcement->created_at->format('d.m.Y')}}</i>
            </div>
          </div>
        </div>

        <div class="mb-4 text-end">
          <a href="{{route('homepage')}}" class=" btn btn-annulla text-white"><i class="fa-solid fa-house-chimney"></i></a>
      </div>
      </div>
</x-layout>