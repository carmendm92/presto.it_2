<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center my-5">
                <h1>{{ __('ui.categoria')}}: {{$category->name}}</h1>
            </div>
            
            @foreach ($announcements as $announcement)
                    <div class="col-12 col-md-3 my-5">
                        <x-card
                        nome="{{$announcement->name}}"
                        immagine="{{$announcement->images->first()?$announcement->images->first()->getUrl(400,400) : 'https://picsum.photos/400'}}"
                        descrizione="{{$announcement->body}}"
                        autore="{{$announcement->user->name}}"
                        datacreazione="{{$announcement->created_at->format('d.m.Y')}}"
                        routeCategoria="{{route('announcement.category', ['category'=>$announcement->category->id])}}"
                        categoria="{{$announcement->category->name}}"
                        route="{{route('announcement.show', compact('announcement'))}}"
                        prezzo="{{$announcement->price}}"
                        >
                        </x-card>
                    </div>
            @endforeach
            <div class="mb-4 text-end">
                <a href="{{route('homepage')}}" class=" btn btn-annulla text-white"><i class="fa-solid fa-house-chimney"></i></a>
            </div>
        </div>
    </div>
</x-layout>