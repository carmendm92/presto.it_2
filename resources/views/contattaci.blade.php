<x-layout>


    <div class="container">
      <div class="row justify-content-center align-items-center flex-wrap p-4">
        <h1 class="text-center p-2">{{ __('ui.contattaci')}}</h1>
        <div class="col-8">
            
          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
        </div>
    
    
        <div class="col-12 col-md-8">
          <form method="POST" action="{{route('user.contact.submit')}} " class="container-form">
          @csrf
            <div class="mb-3">
              <label for="exampleInputname" class="form-label">{{ __('ui.Nome e Cognome')}}</label>
              <input type="text" class="form-control border-success" id="exampleInputname"name="name" value="{{Auth::user()->name}}">
            </div>
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Email</label>
              <input type="text" class="form-control border-success" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" value="{{Auth::user()->email}}">
            </div>
            <div class="mb-3">
            <label for="exampleFormControlTextarea1" class="form-label">{{ __('ui.perchè')}}</label>
              <textarea class="form-control border-success" id="exampleFormControlTextarea1" name="message" rows="3"></textarea>
            </div>
            <div class="text-center mt-4">
              <button type="submit" class="btn btn-invia">Invia</button>
            </div>
          </form>
          
        </div>
      </div>
    </div>
    
    
    
    
    </x-layout>