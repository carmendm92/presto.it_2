<x-layout>
@if($announcement)
<div class="container">
    <div class="row">
        <div class="col-12 my-5">
            <div class="card border border-warning">
                <div class="card-header card_title border border border-warning text-white fw-bolder">{{ __('ui.revisionare')}}</div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-2"><h3>{{ __('ui.utente')}}</h3></div>
                        <div class="col-md-10">
                            <ul class="user-description">
                                <li>Id:{{$announcement->user->id}}</li>
                                <li>{{ __('ui.Nome e Cognome')}}: {{$announcement->user->name}}</li>
                                <li>Email:{{$announcement->user->email}}</li>
                            </ul>
                        </div>
                    </div>

                    <hr class="text-warning">

                    <div class="row">
                        <div class="col-md-2 "><h3>{{ __('ui.titolo')}}</h3></div>
                        <div class="col-md-10 fw-bolder">{{$announcement->name}}</div>
                    </div>

                    <hr class="text-warning">

                    <div class="row">
                        <div class="col-md-2"><h3>{{ __('ui.descrizione')}}</h3></div>
                        <div class="col-md-10">{{$announcement->body}}</div>
                    </div>

                    <hr class="text-warning">

                    <div class="row">
                        <div class="col-md-2"><h3>{{ __('ui.immagini')}}</h3></div>
                        <div class="col-12 col-md-10">
                            @foreach ($announcement->images as $image)
                                
                            
                            <div class="row justify-content-space-between align-items-center">
                                <div class="col-12 col-md-6 container_img">
                                    <img class="img-revisor border border-dark"src="{{$image->getUrl(600,450)}}">
                                    <div>Id:<em>{{$image->id}}</em></div>  
                                </div>
                                
                                <div class="col-12 my-5 col-md-4 d-flex flex-column container-visonsafe">
                                       <div class="d-flex visionsafe">
                                           <span>Adult: </span>
                                           <p class="list-allert {{$image->adult}}">
                                            </p>
                                        </div>
                                        <div class="d-flex visionsafe">
                                            <span>Spoof: </span>
                                            <p class="list-allert {{$image->spoof}}">
                                            </p>
                                         </div>
                                         <div class="d-flex visionsafe">
                                            <span>Medical: </span>
                                            <p class="list-allert {{$image->medical}}">
                                             </p>
                                         </div>
                                         <div class="d-flex visionsafe">
                                            <span>Violence: </span>
                                            <p class="list-allert {{$image->violence}}">
                                             </p>
                                         </div>
                                         <div class="d-flex visionsafe">
                                            <span>Racy: </span>
                                            <p class="list-allert {{$image->racy}}">
                                             </p>
                                            </div>
                                </div>
                                <div class="col-12 my-4">
                                    <h3>Labels</h3> 
                                    @if ($image->labels)

                                        @foreach ($image->labels as $label)
                                            <span>{{$label}} - </span>
                                        @endforeach
                                        
                                    @endif
                                    <hr class="text-warning">

                                  
                
                                </div>
                            </div>
                            @endforeach
                        </div>

                    </div>

                    

                </div>
            </div>
        </div>
    </div>
    <div class="row justify-space-between my-5">
        <div class="col-6">
            <form action="{{route('revisor.reject', $announcement->id)}}" method="POST">
            @csrf
            <button type="submit" class="btn btn-elimina">{{ __('ui.rifiuta')}}</button></form>
        </div>

        <div class="col-6">
            <form action="{{route('revisor.accept', $announcement->id)}}" method="POST" class="text-end">
            @csrf
            <button type="submit" class="btn btn-ripristina ">{{ __('ui.accetta')}}</button></form>
        </div>
    </div>
</div>

@else
<div class="container">
    <div class="row justify-content-center">
        <div class="col-6 my-5 text-center">
            <h2>{{ __('ui.complimenti')}}!</h2>
            <h3>{{ __('ui.zero')}}</h3>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row justify-content-end">
            <div class="text-end">
                <a class="btn btn-invia my-2" href="{{route('softdelete', $announcement)}}"><i class="fa-regular fa-trash-can"></i></a>
            </div>
        </div>
    </div>
</div>
@endif

</x-layout>