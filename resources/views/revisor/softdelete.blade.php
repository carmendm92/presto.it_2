<x-layout>
@if(count($announcements)) 

    <div class="container">
        <div class="row justify-content-center my-2">
           
            @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif

            @if(session('message'))
            <div class="alert alert-warning">
                {{ session('message') }}
            </div>
            @endif

            <div class="col-12 p-4 text-center fw-bolder mt-5">
                <h1>
                    {{ __('ui.rifiutati')}}
                </h1>
            </div>

            @foreach ($announcements as $announcement)
            <div class="col-12 col-md-6 col-xl-3 my-5">

                <div class="border border-dark card">

                    <div class="row justify-content-center align-items-center">
                        <div class="col-12">
                            <h4 class="text-center card_title_delete fw-bolder p-2 m-2">
                                {{$announcement->name}}
                            </h4>
                        </div>
                        <div class="col-12 col-md-6 text-center">
                            <div class="text-center d-flex justify-content-center align-items-center">
                                <img src="{{$announcement->images->first()?$announcement->images->first()->getUrl(400,400) : 'https://picsum.photos/400'}}" class="text-center p-1 border border-dark" alt="{{$announcement->name}}" style="max-height:150px">
                            </div>
                        </div>
                        <div class="col-12 col-md-8 text-center">
                            <a href="{{route('announcement.category', ['category'=>$announcement->category->id])}}" class="btn text-decoration-underline">#{{$announcement->category->name}}</a>
                            <div class="text-center mb-2">
                                <a class="p-2 btn btn-leggi fw-bolder" href="{{route('announcement.show', compact('announcement'))}}">{{ __('ui.leggi')}}</a>
                                <div class="p-2 text-center">
                                    <p>prezzo<br></p>
                                    <p class="text-danger">{{$announcement->price}}€ </p>
                                </div>
                                <i>{{$announcement->created_at->format('d.m.Y')}} - {{$announcement->user->name}}</i>
                            
                                <form action="{{route('unreject', compact('announcement'))}}" method="POST" class="my-2">
                                    @csrf
                                    @method("PUT")
                                    <button class="btn btn-ripristina" method="submit">{{ __('ui.ripristina')}}</button>
                                </form>
                
                                <form action="{{route('delete', compact('announcement'))}}" method="post">
                                    @csrf
                                    @method("delete")
                                    <button class="btn btn-elimina">{{ __('ui.elimina')}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

@else(!$announcements)
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 my-5 text-center">
                <h2> {{ __('ui.vuoto')}}</h2>
                <a href="{{route('homepage')}}" class=" btn btn-annulla text-white"><i class="fa-solid fa-house-chimney"></i></a>
            </div>
        </div>
    </div>
@endif
    </x-layout>