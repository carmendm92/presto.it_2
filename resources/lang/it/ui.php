<?php

return [

    'categorie' => 'Categorie',
    'registrati'=> 'Registrati',
    'prezzo'=>'Prezzo',
    'inserisci'=> 'Inserisci annuncio',
    'recenti' => 'Annunci recenti',
    'pannello'=> 'Pannello di controllo',
    'saluta'=> 'Ciao',
    'cerca'=> 'Cerca',
    'titolo'=> 'Titolo',
    'leggi'=> 'Leggi annuncio',
    'lavora'=> 'Lavora con noi',
    'rifiutati' => 'Annunci rifiutati',
    'vuoto'=> 'Il cestino è vuoto',
    'Inserisci il titolo'=>'Inserisci il titolo',
    'Inserisci descrizione'=>'Inserisci descrizione',
    'Inserisci immagini'=>'Inserisci immagini',
    'Seleziona la categoria'=>'Seleziona la categoria',
    'Inserisci prezzo'=>'Inserisci prezzo',
    'invia'=>'Invia',
    'annulla'=>'Annulla',
    'descrizione prodotto'=>'Descrizione prodotto',
    'Annunci per la categoria'=> 'Annunci per la categoria',
    'Inserisci la tua Email'=> 'Inserisci la tua Email',
    'Inserisci la Password'=>'Inserisci la Password',
    'inserisci il tuo nome'=> 'Inserisci il tuo nome',
    'conferma la tua passowrd'=>'Conferma la tua passowrd',
    'Nome e Cognome'=>'Nome e Cognome',
    'perchè'=>'Perchè vuoi lavorare con noi?',
    'contattaci'=>'Contattaci',
    'risultati'=>'Risultati per ricerca',
    'descrizione'=>'Descrizione',
    'immagini'=>'Immagini',
    'complimenti'=>'Complimenti',
    'zero'=>'Non ci sono più annunci da revisionare',
    'accetta'=>'Accetta',
    'rifiuta'=>'Rifiuta',
    'utente'=>'Utente',
    'revisionare'=>'Annuncio da revisionare',
    'categoria'=>'Annunci per categoria',
    'cestino'=>'Cestino',
    'lingua'=>'Lingua',
    'elimina'=>'Elimina',
    'ripristina'=>'Ripristina',


    


];