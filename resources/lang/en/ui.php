<?php

    return [

        'categorie' => 'Categories',
        'registrati'=> 'Sign in',
        'prezzo'=> 'Price',
        'inserisci' => 'Insert announcement',
        'recenti'=> 'Recent announcements',
        'pannello' => 'Control Panel',
        'saluta'=> 'Hi',
        'cestino'=> 'Basket',
        'cerca'=> 'Search',
        'leggi'=> 'Read announcement',
        'lavora'=> 'Work with us',
        'Titolo'=> 'Title',
        'rifiutati'=> 'Rejected ads',
        'vuoto'=> 'The basket is empty',
        'Inserisci il titolo'=>'Enter Title',
        'Inserisci descrizione'=>'Enter Description',
        'Inserisci immagini'=>'Enter Images',
        'Seleziona la categoria'=>'Select a Category',
        'Inserisci prezzo'=>'Enter Price',
        'invia'=>'Send',
        'annulla'=>'Cancel',
        'descrizione prodotto'=> 'Product description',
        'Annunci per la categoria'=> 'Announcement by category',
        'Inserisci la tua Email'=> 'Enter your email',
        'Inserisci la Password'=>'Enter password',
        'inserisci il tuo nome'=> 'Enter your name',
        'conferma la tua passowrd'=>'Confirm your password',
        'Nome e Cognome'=> 'Name and Surname',
        'perchè'=>'Why do you want to work with us?',
        'contattaci'=>'Contact us',
        'risultati'=>'Search results',
        'descrizione'=>'Description',
        'immagini'=>'Images',
        'complimenti'=>'Congratulations',
        'zero'=>'There are no more announcements to review',
        'accetta'=>'Accept',
        'rifiuta'=>'Reject',
        'utente'=> 'User',
        'revisionare'=>'Announcement to be revised',
        'categoria'=>'Ads by category',
        'lingua'=>'Language',
        'elimina'=>'Delete',
        'ripristina'=>'Restore',

        
        
        
        
        
        





    ];