<?php

namespace App\Models;

use App\Models\User;
use App\Models\Category;
use Laravel\Scout\Searchable;
use App\Models\AnnouncementImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Announcement extends Model
{
    use Searchable;
    
   protected $fillable = [
       'name',
       'body',
       'price',
       'user_id',
       'category_id'
   ];

   public function toSearchableArray()
    {
        $category= $this->category;

        $array = [
            'category'=>$category,
            'id' => $this->id,
            'name' => $this->name,
            'body' => $this->description,
            'price'=>$this->price,
        ];

        

        return $array;
    }

   public function user(){
    return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function images(){
        return $this->hasMany(AnnouncementImage::class);
    }

    static public function ToBeRevisionedCount(){
        return Announcement::where('is_accepted', null)->count();
    }
}

