<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index(){

        $announcements = Announcement::where('is_accepted', true)
        ->orderBy('created_at', 'DESC')
        ->take(5)
        ->get();

        return view('welcome', compact ('announcements'));
    }

    public function announcementsByCategory ($id){
        $category = Category::find($id);
        $announcements = $category -> announcements()
        ->where('is_accepted', true)
        ->orderBy('created_at')
        ->paginate(5);

        return view('announcement.announcementsCategory', compact('category', 'announcements'));
    }


    public function show(Announcement $announcement){
        
        return view ('announcement.show', compact('announcement'));
    }


    public function search(Request $request){
        $q = $request->input('q');

        $announcements = Announcement::search($q)->get();

        return view ('search_result', compact('q', 'announcements'));
    }

    public function locale($locale){
        session()->put('locale', $locale);
        return redirect()->back();
    }
}
