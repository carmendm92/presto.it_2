<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Mail\ContactMail;
use App\Http\Requests\FormRequest;
use App\Http\Requests\StoreRequest;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    public function Form(){
        return view('contattaci');
    }

    public function submit(StoreRequest $request){
        $contact = Contact::create([
            'email'=>$request->email,
            'name'=>$request->name,
            'message'=>$request->message
        ]);

        Mail::to($contact->email)->send(new ContactMail ($contact));
        return redirect (route('homepage'))->with('submit', 'La tua richiesta è stata inoltrata, ti risponderemo al più presto!');
    }
}
