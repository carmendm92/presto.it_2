<?php

namespace App\Http\Controllers;



use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Jobs\GoogleVisionRemovedFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Http\Requests\AnnouncementRequest;

class AnnouncementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Request $request){

        $categories = Category::all();
        $uniqueSecret = $request->old(
            'uniqueSecret',
            base_convert(sha1(uniqid(mt_rand())), 16, 36)
        );
        return view('announcement.create', compact('uniqueSecret'));
    }


    public function store(AnnouncementRequest $request){
      
        $announcement= Auth::user()->announcements()->create([
             
            'name'=>$request->name,
            'body'=>$request->body,
            'price'=>$request->price,
            'category_id'=>$request->category_id,
            
        ]);

        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}",[]);
        $removedImages = session()->get("removedimages.{$uniqueSecret}",[]);
        
        $images= array_diff($images, $removedImages);

        foreach($images as $image){
            $i = new AnnouncementImage();
            
            $fileName = basename($image);
            $newFileName = "public/announcements/{$announcement->id}/{$fileName}";
            Storage::move($image, $newFileName);
       
            $i->file = $newFileName;
            $i->announcement_id = $announcement->id;

            $i->save();

            GoogleVisionSafeSearchImage::withChain([
                new GoogleVisionLabelImage($i->id),
                new GoogleVisionRemovedFaces($i->id),
                new ResizeImage($i->file,400,400),
                new ResizeImage($i->file,600,450),
                new ResizeImage($i->file,290,400)
            ])->dispatch($i->id);
        }

        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));

        return redirect(route('homepage'))->with('message', 'il tuo annuncio è stato inserito');

    }


    public function uploadImage(Request $request){
        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");


        dispatch(new ResizeImage(
            $fileName,
            120,
            120
        ));


        session()->push("images.{$uniqueSecret}", $fileName);
        return response()->json(
            [
            'id' =>$fileName
            ]
        );
        
    }


    public function removeImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->input('id');

        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);
        
        return response()->json('ok');
    }


    public function getImages(Request $request){
        $uniqueSecret = $request->input('uniqueSecret');
        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        $data =[];

        foreach ($images as $image){
            $data[] = [
                'id' =>$image,
                'src' =>AnnouncementImage::getUrlByFilePath($image,120,120),
            ];
        }
        return response()->json($data);
    }
}
