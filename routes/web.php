<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/inserisci-annuncio', [AnnouncementController::class, 'create'])->name('announcement.create');

Route::post('/crea-annuncio', [AnnouncementController::class, 'store'])->name('announcement.store');

Route::post('/announcement/images/upload', [AnnouncementController::class, 'uploadImage'])->name('announcement.image.upload');

Route::delete('/announcement/images/remove', [AnnouncementController::class, 'removeImage'])->name('announcement.image.remove');

Route::get('/announcement/images/', [AnnouncementController::class, 'getImages'])->name('announcement.images');


Route::get('/', [PublicController::class, 'index'])->name('homepage');


Route::get('/category/{category}/announcements', [PublicController::class, 'announcementsByCategory'])->name('announcement.category');

Route::get('/dettaglio-annuncio/{announcement}', [PublicController::class, 'show'])->name('announcement.show');

Route::get('/cerca', [PublicController::class, 'search'])->name('search');

Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name('locale');



Route::get('/revisor/home', [RevisorController::class, 'index'])->name('homepage.revisor');

Route::post('/revisor/announcement/{id}/accept', [RevisorController::class, 'accept'])->name('revisor.accept');

Route::post('/revisor/announcement/{id}/reject', [RevisorController::class, 'reject'])->name('revisor.reject');

Route::get('/revisor/cestino', [RevisorController::class, 'softdelete'])->name('softdelete');

Route::put('/revisor/ripristina/{announcement}',[RevisorController::class, 'unreject'])->name('unreject');

Route::delete('/revisor/elimina/{announcement}', [RevisorController::class, 'delete'])->name('delete');





Route::get('/contattaci', [FormController::class, 'Form'])->name('user.contact');

Route::Post('/contattaci/submit', [FormController::class, 'submit'])->name('user.contact.submit');


